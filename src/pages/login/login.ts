import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SecondPage } from '../../pages/second/second';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
 str1="button2";
 input1:any;
 labels=[ {name:"lab1",kk:"slem"},{name:"lab2",kk:"erbil"},{name:"lab3",kk:"dhok"} ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.str1 = "btn3";

  }
  public fnc1()
  {

  	this.str1=this.input1;
  	console.log("hello");
  }
  public fnc2(i:number)
  {
  	console.log(i+ " clicked");
  }
  public fnc3(t:any)
  {
  	console.log(t);
  	this.str1=t;
  }
  public fun4()
  {
  	// this.navCtrl.push(SecondPage);
  	this.navCtrl.setRoot(SecondPage,{abas:this.labels});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
