import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-second',
  templateUrl: 'second.html',
})
export class SecondPage {
	labels:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.labels =  this.navParams.get("abas");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SecondPage');
  }

}
